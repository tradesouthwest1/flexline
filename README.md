# Flexline

Requires PHP: 7.2
Requires CP:  1.4
Streamlined fast loading theme for ClassicPress with mobile ready menus and widgets.

## Features
Flexline Theme Settings
Changes height of logo in header
Choose the font family type.
Add HTML to Header just above description.
Color of Background in Header Footer
Color for Links
Excerpts show in featured image area above post.


![flexline theme](https://classicpress-themes.com/wp-content/uploads/2023/07/screenshot.png)
